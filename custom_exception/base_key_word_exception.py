#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
@Project ：uimonkey 
@File    ：base_key_word_exception.py
@IDE     ：PyCharm 
@Author  ：ShiLi
@Date    ：2022/4/15 9:05 下午 
"""


class WebDriverPathException(Exception):
    """
    获取web driver 驱动路径失败
    """
    pass


class NotFoundElementException(Exception):
    """
    获取元素失败
    """
    pass


class GetWindowsHandleException(Exception):
    """
    未获取到窗口句柄
    """
    pass
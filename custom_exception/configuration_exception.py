#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
@Project ：uimonkey 
@File    ：configuration_exception.py
@IDE     ：PyCharm 
@Author  ：ShiLi
@Date    ：2022/4/16 9:47 上午 
"""


class CaseIsEmptyException(Exception):
    """case 目录下没有用例"""
    pass

class GetConfigurationException(Exception):
    """
    获取配置异常
    """
    pass
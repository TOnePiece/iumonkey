#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
@Project ：uimonkey 
@File    ：log_constant.py
@IDE     ：PyCharm 
@Author  ：ShiLi
@Date    ：2022/4/16 10:04 上午 
"""
import os

from constant.base_path import BASE_PATH

LOGS_DIR = os.path.join(BASE_PATH, "logs")
ROTATION = "500MB"
RETENTION = "10 days"

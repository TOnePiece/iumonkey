#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
@Project ：uimonkey 
@File    ：base_path.py
@IDE     ：PyCharm 
@Author  ：ShiLi
@Date    ：2022/4/16 9:52 上午 
"""

import os

AllURE_SERVICE_PORT = 8883

BASE_PATH = os.path.abspath(".")

# 用例配置路径
CASE_PATH = os.path.join(BASE_PATH, "case")

REPORT_TEMP_DATA_DIR = os.path.join(BASE_PATH, "temp_data")
REPORT_DIR = os.path.join(BASE_PATH, "report")

# 用例po 步骤路径
CASE_PO_PATH = os.path.join(BASE_PATH, "po")

# 用例参数路径
CASE_PARAMS_PATH = os.path.join(BASE_PATH, "case_params")

# 基础配置路径
BASE_CONFIG_PATH = os.path.join(BASE_PATH, "config/base_config.yaml")

# 数据库配置路径
DB_CONFIG_PATH = os.path.join(BASE_PATH, "config/db_config.yaml")

# wenDriver 配置路径

# linux 系统
CHROME_WEB_DRIVER_PATH = os.path.join(BASE_PATH, "tools/chromedriver")
FIREFOX_WEB_DRIVER_PATH = os.path.join(BASE_PATH, "tools/firefoxdriver")
IE_WEB_DRIVER_PATH = os.path.join(BASE_PATH, "tools/iedriver")

# Windows 系统
W_CHROME_WEB_DRIVER_PATH = os.path.join(BASE_PATH, "tools/chromedriver.exe")
W_FIREFOX_WEB_DRIVER_PATH = os.path.join(BASE_PATH, "tools/firefoxdriver.exe")
W_IE_WEB_DRIVER_PATH = os.path.join(BASE_PATH, "tools/iedriver.exe")

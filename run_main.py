#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
@Project ：uimonkey 
@File    ：rum_main.py
@IDE     ：PyCharm 
@Author  ：ShiLi
@Date    ：2022/4/16 12:03 下午 
"""
import os
import subprocess
import time

import pytest

from constant.base_path import REPORT_TEMP_DATA_DIR, REPORT_DIR, AllURE_SERVICE_PORT


def create_report_dit():
    if not os.path.exists(REPORT_TEMP_DATA_DIR):
        os.makedirs(REPORT_TEMP_DATA_DIR)
    if not os.path.exists(REPORT_DIR):
        os.makedirs(REPORT_DIR)


def run_report_service():
    create_report_command = "allure generate {0} -o {1} --clean".format(REPORT_TEMP_DATA_DIR, REPORT_DIR)
    command = "nohup allure open  -h 127.0.0.1 -p {0} {1} >out.file 2>&1 &".format(AllURE_SERVICE_PORT, REPORT_DIR)
    status, res = subprocess.getstatusoutput(create_report_command)
    status, res = subprocess.getstatusoutput(command)


if __name__ == '__main__':
    create_report_dit()
    pytest.main(["--browser=chrome", "--alluredir={0}".format(REPORT_TEMP_DATA_DIR), "--cache-clear"])
    time.sleep(3)
    run_report_service()
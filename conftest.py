#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
@Project ：uimonkey 
@File    ：conftest.py
@IDE     ：PyCharm 
@Author  ：ShiLi
@Date    ：2022/4/16 3:45 下午 
"""
import platform
import pytest
from selenium import webdriver
from constant.base_path import CHROME_WEB_DRIVER_PATH, FIREFOX_WEB_DRIVER_PATH, IE_WEB_DRIVER_PATH, \
    W_IE_WEB_DRIVER_PATH, W_FIREFOX_WEB_DRIVER_PATH, W_CHROME_WEB_DRIVER_PATH, BASE_CONFIG_PATH
from custom_exception.base_key_word_exception import WebDriverPathException
from custom_exception.configuration_exception import GetConfigurationException
from key_word.parse_case import ParseCase
from utils.read_yaml_utils import ReadYamlUtils


def pytest_addoption(parser):
    """
    添加自定义参数
    :param parser:
    """
    parser.addoption("--env", action="store", default=None, help=" 使用的环境值 --env ")
    parser.addoption("--browser", action="store", default="chrome", help="测试使用的浏览器 --chrome ")


@pytest.fixture(scope="session")
def custom_env(request) -> str:
    """
    获取自定义参数 env的值
    :param request:
    :return:
    """
    return request.config.getoption("--env")


@pytest.fixture
def custom_browser(request) -> str:
    """
    获取自定义参数   browser的值
    :param request:
    :return:
    """
    return request.config.getoption("--browser")


def browser_config(browser="Chrome") -> dict:
    """
    1、根据基础配置文件 或 传入的参数 获取使用的浏览器类型和 webDriver
    2、判断系统类型，windows 和 linux webDriver 名称不一样
    :return: dict
    """
    driver_path_dict = {
        "Linux": {"Chrome": CHROME_WEB_DRIVER_PATH, "Firefox":FIREFOX_WEB_DRIVER_PATH, "Ie": IE_WEB_DRIVER_PATH},
        "Windows": {"Chrome": W_CHROME_WEB_DRIVER_PATH, "Firefox": W_FIREFOX_WEB_DRIVER_PATH, "Ie": W_IE_WEB_DRIVER_PATH}}
    # 获取当前系统名称
    system_platform = platform.system()
    # 除windows 外的 都假设他是 linux系统
    if system_platform != "Windows":
        system_platform = "Linux"

    if not browser:
        run_config_data = ReadYamlUtils.read_yaml(BASE_CONFIG_PATH)
        browser = run_config_data.get("browser")
        if not browser:
            raise GetConfigurationException("从配置中没有获取到 browser 配置")
    # 首字符大写
    new_browser = browser.capitalize()

    platform_web_driver_dict = driver_path_dict.get(system_platform)
    driver_path = platform_web_driver_dict.get(new_browser)
    if not driver_path:
        raise WebDriverPathException()
    driver_details = {"browser": new_browser, "driver_path": driver_path}
    return driver_details


@pytest.fixture(scope="function")
def web_driver(custom_browser) -> webdriver:
    """
    :param custom_browser: 自定义 浏览器驱动的值
    """
    driver_details = browser_config(custom_browser)
    browser = driver_details.get("browser")
    driver_path = driver_details.get("driver_path")
    driver = getattr(webdriver, browser)(executable_path=driver_path)
    driver.implicitly_wait(1)
    driver.maximize_window()
    yield driver
    driver.quit()


@pytest.fixture(autouse=True, params=ParseCase.get_case_details(), scope="function")
def parse_case_details(request, web_driver) -> None:
    """
    测试用例解析并执行的 fixture  可以理解为主测试方法
    :param request:
    :param web_driver:
    """
    case_details = request.param
    ParseCase.parse_case(case_details, web_driver)

# UIMonkey

#### 介绍
基于  pytest的web UI 关键字驱动测试

#### 软件架构
* 整体使用 po + 关键字驱动的模型设置集po 模式和 关键字的所有优点

![](architecture_image/img.jpg)


#### 安装教程
* python3 的版本
* [自己下载 allure 并配置 环境变量 --->教程](https://blog.csdn.net/m0_49225959/article/details/117194318)
* 根据自己电脑的型号和浏览器版本型号下周 webDriver 驱动  并放到 tools 目录下， 驱动路径配置在 config/base_config 目录下， 如果需要重新指定 webdriver 的地址直接修改后面的值value，不要修改前面的key
* 依赖安装
```shell
pip install -r requirements.txt  -i https://pypi.mirrors.ustc.edu.cn/simple/ 
```

#### 使用说明
##### 很重要

* `test_template.py` 文件不要删除，这个是一个模板文件，删除后会导致pytest无法使用
* 如果是windows 系统 请在 run_main.py 注释： status, res = subprocess.getstatusoutput(command) 这行代码， 应该不支持nohup


#### 项目使用步骤：
第一步： 先下载 webDriver驱动 。。。。。。。。
第二步：
* 首先你要了解 yaml 文件的语法 特别是 文件数据引用，这个很有必要， 
* 例子---->  标记：index_login_button: &index_login_button  引用 <<: *index_login_button
---
* 1、 编写 po 下的页面业务逻辑文件
```yaml
index_login_button: &index_login_button
    element: "//a[@name='tj_login' and @class='s-top-login-btn c-btn c-btn-primary c-btn-mini lb']"

po:
  login:
      event: "当前是在登录页面"
      step:
        - funcType: get_url
          params: https://www.baidu.com
          event: 打开浏览器

        - customFunc: random_int
          event: 随机生成一个整数

        - funcType: click
          findType: Xpath
          <<: *index_login_button
          event: 在首页点击登录按钮

#          - funcType: send_keys
#            findType: Xpath
#            element: "//input[@id='TANGRAM__PSP_11__userName']"
#            params: "${UserConfig.yaml: env:username}"
#            event: 输入登录的账号

        - funcType: send_keys
          findType: Xpath
          element: "//input[@id='TANGRAM__PSP_11__userName']"
          params: "last_custom_func_result"
          event: 输入登录的账号

        - funcType: send_keys
          findType: Xpath
          element: "//input[@id='TANGRAM__PSP_11__password']"
          params: "${UserConfig: env:username}"
          event: 输入登录的密码

        - funcType: click
          findType: Xpath
          element: "//input[@id='TANGRAM__PSP_11__submit']"
          event: 点击登录

      assertData:
        - funcType: findElement
          findType: Xpath
          <<: *index_login_button
          assert_func: not_find_element

```

* 2、编写case 文件
```yaml
cases_spet: &cases_spet
    baidu->login.yaml:
      - po.login
    baidu->search.yaml:
      - po.search

case:
  - case_name: 用例111
    source_case_params: baidu->search_params.yaml
    <<: *cases_spet

  -  case_name: 用例222
     source_case_params: baidu->search_params.yaml2
     <<: *cases_spet

```

* 3、编写 case_params 文件：(其实参数数据读取没有实现) 所以 。。。。。
```yaml
params:
  - case_name: 百度搜索-- 好的
    data:
      params:  好的

  - case_name: 百度搜索-- 好的22
    data:
      params:  好的22
```

##### 如何使用自定义函数的返回值
* 关键值是 params 后面的value = last_custom_func_result
> 在 po 的文件夹下 yaml 中使用
```yaml

        - customFunc: random_int
          event: 随机生成一个整数

        - funcType: send_keys
          findType: Xpath
          element: "//input[@id='TANGRAM__PSP_11__userName']"
          params: "last_custom_func_result"
          event: 输入登录的账号

```
输入登录账号 的 params 的值就会是上面自定义方法 random_int 的返回值

#### 二次开发
* 关键字文件 key_word/base_key_word.py
* 目前已有的关键字：
  * get_url
  * discover_element
  * find_element
  * find_elements
  * click
  * send_keys
  * set_windows_size
  * switch_windows
  * quit
---
* 自定义函数文件 
  utils/custom_func.py
* 目前已有
  * sleep
  * current_time
  * current_time_stamp
  * random_float
  * random_int
---
* 组装用例文件
 key_word/parse_case.py
* get_case_list 读取 case 目录下的所有文件并返回 一个list
* get_case_details 从上面的list中 读取到所有的用例步骤 并返回一个list
* parse_case 从上面的 list 中 执行所有的用例（非常重要的一个方法）

#### 未完成功能
- 用例文件的数据读取
- 文件格式数据检查
- flask 接口调用

> java 版本 有时间搞

#### 设想

> 使用docker 多节点 实现可同时执行多个浏览器

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
@Project ：uimonkey 
@File    ：parse_case_file.py
@IDE     ：PyCharm 
@Author  ：ShiLi
@Date    ：2022/4/16 5:55 下午 
"""
import os
import re
from constant.base_path import CASE_PATH
from custom_exception.configuration_exception import CaseIsEmptyException
from key_word.base_key_word import BaseKeyWord
from utils.custom_func import CustomFunc
from utils.get_data_node_utils import GetDataNodeUtils
from utils.read_yaml_utils import ReadYamlUtils
from utils.log_utils import Logger


class ParseCase(object):
    logger = Logger()
    pattern = re.compile(r"\${custom:(.*)}")

    @classmethod
    def get_case_list(cls) -> list:
        """
        遍历用例文件夹下的所有用例文件， 重新放到一个list
        :return:  list
        """
        case_path = CASE_PATH
        case_list = os.listdir(case_path)
        dist_case_list = list()
        if not case_list:
            raise CaseIsEmptyException("case 目录为空 请在：{} 添加.yaml结尾的用例".format(case_path))
        for file in case_list:
            # 检查文件是不是已yaml 结尾
            if file.endswith(".yaml"):
                full_file_path = os.path.join(case_path, file)
                dist_case_list.append(full_file_path)
        if not dist_case_list:
            raise CaseIsEmptyException("case 目录下没有.yaml结尾的用例 请在：{} 添加.yaml结尾的用例".format(case_path))
        return dist_case_list

    @classmethod
    def get_case_details(cls) -> list:
        """
        根据所有的用例文件夹、遍历出所有的 用例步骤信息， 添加到 case_list_details 列表
        :return:
        """
        dist_case_list = cls.get_case_list()
        case_list_details = list()
        for case_file in dist_case_list:
            case_file_data = ReadYamlUtils.read_yaml(case_file)
            [case_list_details.append(case) for case in case_file_data.get("case")]
        return case_list_details

    @classmethod
    def parse_case(cls, case_details, driver):
        for key, value in case_details.items():
            if key.endswith(".yaml"):
                case_data_sept_details = GetDataNodeUtils.get_sept_file_data_utils(key)
                # 循环处理 case 下 yaml文件的 value 节点信息
                for v in value:
                    node_data_details = GetDataNodeUtils.get_data_node_utils(case_data_sept_details, v)
                    cls.logger.info("==================开始进入： {}==================".format(node_data_details.get("event")))
                    step_list = node_data_details.get("step")
                    result = None
                    for step in step_list:
                        cls.logger.info("当前步骤：{}".format(step.get("event")))
                        func_type = step.get("funcType")
                        params_dict = dict()
                        find_type = step.get("findType")
                        element = step.get("element")
                        params = step.get("params")
                        custom_func = step.get("customFunc")
                        if find_type:
                            params_dict["find_type"] = find_type.upper()
                        if element:
                            params_dict["element"] = element

                        if params == "last_custom_func_result":
                            params = result

                        try:
                            params_key = cls.pattern.findall(string=params)[0].split(" ")[-1]
                            source_case_params = case_details.get("source_case_params")
                            params = source_case_params.get(params_key)
                        except:
                            pass

                        if params:
                            params_dict["params"] = params

                        if func_type:
                            getattr(BaseKeyWord(driver), func_type)(**params_dict)
                        elif custom_func:
                            custom_func_result = getattr(CustomFunc, custom_func)()
                            if custom_func_result:
                                result = custom_func_result
                        else:
                            raise Exception("没有方法")
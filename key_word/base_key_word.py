#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
@Project ：uimonkey 
@File    ：base_key_word.py
@IDE     ：PyCharm 
@Author  ：ShiLi
@Date    ：2022/4/15 6:15 下午 
"""

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import ui
from selenium.webdriver.support import expected_conditions as EC
from custom_exception.base_key_word_exception import NotFoundElementException, GetWindowsHandleException
from utils.log_utils import Logger


class BaseKeyWord(object):
    """
    关键字方法
    """

    def __init__(self, driver):
        self.driver = driver
        self.logger = Logger()

    def get_url(self, params):
        """
        访问测试域名
        :param params:
        """
        self.driver.get(params)

    def discover_element(self, find_type, element, timeout) -> bool:
        """
        :param find_type: 查询类型
        :param element: 元素
        :param timeout: 超时时间
        :return: bool
        """

        try:
            self.logger.info("正在使用：{0}的方式查找元素：{1}".format(find_type, element))
            ui.WebDriverWait(self.driver, timeout=timeout).until(EC.visibility_of_element_located((getattr(By, find_type), element)))
            return True
        except TimeoutException:
            self.logger.error("没有找到元素：{0}".format(element))
            return False

    def find_element(self, find_type: str, element: str, timeout: int = 5):
        """
        :param find_type: 查询类型
        :param element: 元素
        :param timeout: 超时时间
        :return:  element Object
        """
        fiend_result = self.discover_element(find_type=find_type,element=element, timeout=timeout)
        if fiend_result:
            return self.driver.find_element(getattr(By, find_type),value=element)
        else:
            raise NotFoundElementException("获取元素异常： 定位方式：{0}  元素： {1}".format(find_type,element))

    def find_elements(self, find_type: str, element: str, timeout: int = 5) -> list:
        """
        多元素查找
        :param find_type: 查询类型
        :param element: 元素
        :param timeout: 超时时间
        :return:  element Object
        """
        fiend_result = self.discover_element(find_type=find_type,element=element, timeout=timeout)
        if fiend_result:
            return self.driver.find_elements(getattr(By, find_type),value=element)
        else:
            raise NotFoundElementException("获取元素异常： 定位方式：{0}  元素： {1}".format(find_type,element))

    def click(self, find_type: str, element: str, timeout: int = 5):
        """
        点击
        :param find_type: 查询类型
        :param element: 元素
        :param timeout: 超时时间
        """
        result_ele = self.find_element(find_type, element, timeout)
        result_ele.click()

    def send_keys(self, find_type: str, element: str, timeout: int = 5, params: str = None):
        """
        元素输入
        :param find_type: 查询类型
        :param element: 元素
        :param timeout: 超时时间
        :param text: 输入的内容
        """
        result_ele = self.find_element(find_type, element, timeout)
        result_ele.send_keys(params)

    def set_windows_size(self, params):
        """
        设置窗口大小
        :param params:
        """
        width = params.get("width")
        height = params.get("height")
        self.driver.set_window_size(width, height)

    def switch_windows(self, switch_header_index=None):
        driver = webdriver.Chrome()
        # 获取所有的窗口
        windows_list = self.driver.window_handles
        # 获取当前窗口
        current_windows = self.driver.current_window_handle
        # 如果传入了窗口句柄索引，则直接使用， 否则判断是否和当前句柄相同，不相同就使用
        dist_handle = None
        if switch_header_index:
            dist_handle = windows_list[switch_header_index]
        else:
            for handle in windows_list:
                if handle != current_windows:
                    dist_handle = handle
                    break
            # 如果没有找到需要切换的窗口，则抛出异常
            if not dist_handle:
                self.logger.error("获取窗口句柄异常： 全部窗口句柄列表: {}".format(windows_list))
                raise GetWindowsHandleException("获取窗口句柄异常： 全部窗口句柄列表: {}".format(windows_list))
        driver.switch_to.window(dist_handle)

    def quit(self):
        self.driver.quit()
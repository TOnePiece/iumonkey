#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
@Project ：uimonkey 
@File    ：log_utils.py
@IDE     ：PyCharm 
@Author  ：ShiLi
@Date    ：2022/4/16 10:01 上午 
"""



import time
import os
from loguru import logger

from constant.log_constant import LOGS_DIR, ROTATION, RETENTION


def logger_format(level="INFO"):
    t = time.strftime("%Y-%m-%d")
    if level == "ERROR":
        log_path = os.path.join(LOGS_DIR, "error_{0}.log".format(t))
        filter_func = error_only
    else:
        log_path = os.path.join(LOGS_DIR, "{0}.log".format(t))
        filter_func = not_error
    logger.add(log_path,
               rotation=ROTATION,  # 最大保存大小
               encoding="utf-8",  # 支持中文格式
               enqueue=True,  # 支持异步存储
               retention=RETENTION,  # 保存期限10天
               format="[{time:YYYY-MM-DD HH:mm:ss} {level:<8}|{file}:{line}]  {message}",
               filter=filter_func,
               )


def error_only(record):
    return record["level"].name == "ERROR"


def not_error(record):
    return record["level"].name != "ERROR"


class Logger(object):
    __instance = None

    def __new__(cls, *args, **kwargs):
        if not cls.__instance:
            cls.__instance = super(Logger, cls).__new__(cls, *args, **kwargs)

        return cls.__instance

    logger_format()
    logger_format("ERROR")

    def info(self, msg):
        return logger.info(msg)

    def debug(self, msg):
        return logger.debug(msg)

    def warning(self, msg):
        return logger.warning(msg)

    def error(self, msg):
        return logger.error(msg)


if __name__ == '__main__':
    loggings = Logger()
    loggings.error("中文test1")
    loggings.info("中文test2")
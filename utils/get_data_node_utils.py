#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
@Project ：uimonkey 
@File    ：get_data_node_utils.py
@IDE     ：PyCharm 
@Author  ：ShiLi
@Date    ：2022/4/17 4:57 下午 
"""
import os

from constant.base_path import CASE_PO_PATH
from utils.read_yaml_utils import ReadYamlUtils


class GetDataNodeUtils(object):

    @classmethod
    def get_sept_file_data_utils(cls, path):
        """
        步骤路径数据读取
        :param path:
        :return:
        """
        path_node_list = path.split("->")
        node_path = CASE_PO_PATH
        if path_node_list:
            for path_node in path_node_list:
                node_path = os.path.join(node_path, path_node)
        else:
            node_path = os.path.join(path)

        data = ReadYamlUtils.read_yaml(node_path)
        return data

    @classmethod
    def get_data_node_utils(cls, case_data_sept_details, node_string) -> dict:
        """
        获取 po 文件下的 步骤信息
        :param case_data_sept_details:
        :param node_string:
        :return:
        """
        node_list = node_string.split(".")
        for node in node_list:
            case_data_sept_details = case_data_sept_details.get(node)
        return case_data_sept_details

#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
@Project ：uimonkey 
@File    ：read_yaml_utils.py
@IDE     ：PyCharm 
@Author  ：ShiLi
@Date    ：2022/4/15 10:05 下午 
"""
import yaml

from utils.log_utils import Logger


class ReadYamlUtils(object):
    logger = Logger()

    @classmethod
    def read_yaml(cls, path):
        """
        读取 yaml 文件数据
        @param path: yaml 文件路径
        @return:
        """
        with open(path, 'r', encoding='utf8') as file:
            file_data = file.read()
            data = yaml.full_load(file_data)
        cls.logger.info("文件读取的内容:{}".format(data))
        return data
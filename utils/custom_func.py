#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
@Project ：uimonkey 
@File    ：custom_func.py
@IDE     ：PyCharm 
@Author  ：ShiLi
@Date    ：2022/4/16 1:22 下午 
"""
import random
import time


class CustomFunc(object):
    """
    自定义函数供外部用例调用
    """

    @classmethod
    def sleep(cls, params=2):
        time.sleep(params)

    @classmethod
    def current_time(cls):
        """
        获取当前时间
        :return:
        """
        return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))

    @classmethod
    def current_time_stamp(cls):
        """
        获取当前时间戳
        :return:
        """
        return time.time()

    @classmethod
    def random_float(cls):
        return random.random()

    @classmethod
    def random_int(cls):
        """
        随机生成一个大于0的整数
        :return:
        """
        return random.randint(0,10000000000000000)